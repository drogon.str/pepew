from django.db import models
from django.utils import timezone
# Create your models here.
class modell(models.Model):
    nama_event = models.CharField(max_length = 200)
    tanggal_waktu_event = models.DateField()
    waktu_event = models.TimeField()
    tempat_event = models.CharField(max_length = 200)
    kategori_event = models.CharField(max_length = 200)
    