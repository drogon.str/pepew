from django.urls import re_path
from .views import story2, Challenge3, index, fill_form
from django.conf.urls import url

urlpatterns = [
    re_path(r'^$', story2, name="index"),
    re_path(r'Challenge3.html', Challenge3, name="Challenge3"),
    re_path(r'forms.html', fill_form, name="fill_form")
]
