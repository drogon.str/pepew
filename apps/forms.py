from django import forms

class SchedForm(forms.Form):
    attrs = {
        'class': 'form-control'
    }
    attrsDate = {
        'class' : 'form-control',
        'type' : 'date'
    }
    attrsTime = {
        'class' : 'form-control',
        'type' : 'time'
    }

    nama = forms.CharField(label = "Nama Kegiatan", required = True, max_length=50, empty_value="Anonymous", widget=forms.TextInput())
    kategori = forms.CharField(label = "Kategori Kegiatan", required = True, max_length=50, widget = forms.TextInput())
    waktu_tanggal = forms.DateField(label = "Tanggal Kegiatan", required = True, widget=forms.DateInput(attrs=attrsDate))
    waktu = forms.TimeField(label = "Waktu Kegiatan", required = True, widget = forms.TimeInput(attrs=attrsTime))
    tempat = forms.CharField(label = "Tempat Kegiatan", required = True, max_length=50, widget=forms.TextInput())