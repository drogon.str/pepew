from django.shortcuts import render
from django.shortcuts import redirect
from django.http import HttpResponseRedirect
from .forms import SchedForm
from .models import modell

# Create your views here.
response = {'Author' : "Hermawan Arifin"}
def story2(request):
    return render(request, 'story2.html')

def index(request):
    return render(request, 'story2.html')
    
def Challenge3(request):
    return render(request, 'Challenge3.html')

def form(request):
    html  = 'form.html'
    response['sched_form'] = SchedForm
    return render(request, html, response)

def fill_form(request):
    form = SchedForm(request.POST)
    mod = modell.objects.all()
    if (request.method == "POST"):
        if 'delete' in request.POST:
            modell.objects.all().delete()
            context = {
                'form': form,
                'sch' : mod
            }
            return render(request, 'form.html', context)

        response['nama_event'] = request.POST['nama']
        response['waktu_event'] = request.POST['waktu']
        response['tanggal_waktu_event'] = request.POST['waktu_tanggal']
        response['tempat_event'] = request.POST['tempat']
        response['kategori_event'] = request.POST['kategori']

        objMod = modell(nama_event = response['nama_event'], 
                        tanggal_waktu_event = response['tanggal_waktu_event'],
                        tempat_event = response['tempat_event'], 
                        kategori_event = response['kategori_event'], 
                        waktu_event = response['waktu_event'])
        objMod.save()
        mod = modell.objects.all()

        form = SchedForm()
        context = {
            'form': form,
            'sch' : mod
        }
        return render(request, 'form.html', context)

    else:
        form = SchedForm()
        context ={
            'form':form,
        }
        return render(request, 'form.html', context)


    
